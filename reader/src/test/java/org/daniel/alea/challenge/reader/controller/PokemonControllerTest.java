package org.daniel.alea.challenge.reader.controller;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.service.PokemonService;
import org.daniel.alea.challenge.reader.mapper.PokemonWebMapper;
import org.daniel.alea.challenge.reader.response.PokemonResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PokemonControllerTest {

    static PokemonDto ANY_POKEMON_DTO = new PokemonDto(1, "name", 2, 3, 4);
    static PokemonResponse ANY_POKEMON_RESPONSE = new PokemonResponse(1, "name", 2, 3, 4);

    @Mock
    PokemonService pokemonService;

    @Mock
    PokemonWebMapper mapper;

    @InjectMocks
    PokemonController pokemonController;

    @Test
    void interactsWithTheServiceTallest() {
        when(pokemonService.getTallest(anyInt())).thenReturn(Collections.emptyList());
        pokemonController.getTallest(5);

        verify(pokemonService).getTallest(5);
    }

    @Test
    void interactsWithTheServiceHeaviest() {
        when(pokemonService.getHeaviest(anyInt())).thenReturn(Collections.emptyList());
        pokemonController.getHeaviest(5);

        verify(pokemonService).getHeaviest(5);
    }

    @Test
    void returnsCorrectly() {
        when(pokemonService.getWithHighestBaseExperience(anyInt()))
                .thenReturn(List.of(ANY_POKEMON_DTO));
        when(mapper.toResponse(ANY_POKEMON_DTO)).thenReturn(ANY_POKEMON_RESPONSE);

        assertThat(pokemonController.getHighestBaseExp(5)).containsExactly(ANY_POKEMON_RESPONSE);
        verify(pokemonService).getWithHighestBaseExperience(5);
    }
}
