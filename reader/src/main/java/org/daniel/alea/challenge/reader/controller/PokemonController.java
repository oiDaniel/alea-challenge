package org.daniel.alea.challenge.reader.controller;

import lombok.RequiredArgsConstructor;
import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.service.PokemonService;
import org.daniel.alea.challenge.reader.mapper.PokemonWebMapper;
import org.daniel.alea.challenge.reader.response.PokemonResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("pokemon")
@RequiredArgsConstructor
public class PokemonController {

    private final PokemonService pokemonService;
    private final PokemonWebMapper pokemonWebMapper;

    @GetMapping("/heaviest")
    public List<PokemonResponse> getHeaviest(@RequestParam(required = false, defaultValue = "5") int limit) {
        return toDtoList(pokemonService.getHeaviest(limit));
    }

    @GetMapping("/tallest")
    public List<PokemonResponse> getTallest(@RequestParam(required = false, defaultValue = "5") int limit) {
        return toDtoList(pokemonService.getTallest(limit));
    }

    @GetMapping("/highestBaseExp")
    public List<PokemonResponse> getHighestBaseExp(@RequestParam(required = false, defaultValue = "5") int limit) {
        return toDtoList(pokemonService.getWithHighestBaseExperience(limit));
    }

    private List<PokemonResponse> toDtoList(List<PokemonDto> pokemons) {
        return pokemons.stream()
                .map(pokemonWebMapper::toResponse)
                .collect(Collectors.toList());
    }
}
