package org.daniel.alea.challenge.reader.mapper;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.reader.response.PokemonResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PokemonWebMapper {

    PokemonResponse toResponse(PokemonDto pokemonDto);
}
