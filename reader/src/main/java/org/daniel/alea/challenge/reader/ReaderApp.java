package org.daniel.alea.challenge.reader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "org.daniel.alea")
public class ReaderApp {

    public static void main(String[] args) {
        SpringApplication.run(ReaderApp.class, args);
    }
}
