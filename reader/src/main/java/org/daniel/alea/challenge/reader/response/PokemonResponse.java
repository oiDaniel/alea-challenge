package org.daniel.alea.challenge.reader.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

@Data
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class PokemonResponse {

    int id;
    String name;
    int height;
    int weight;
    int baseExperience;
}
