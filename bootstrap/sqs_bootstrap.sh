#!/usr/bin/env bash

# https://stackoverflow.com/a/70063706

echo "Creating queues"

LOCALSTACK=localhost
REGION=eu-west-1

create_queue() {
    local NAME=$1
    awslocal --endpoint-url=http://${LOCALSTACK}:4566 sqs create-queue \
    --queue-name ${NAME} --region ${REGION} --attributes VisibilityTimeout=30
}

create_queue "pokemon-fanout"
