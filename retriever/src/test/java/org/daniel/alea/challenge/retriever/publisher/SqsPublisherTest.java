package org.daniel.alea.challenge.retriever.publisher;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.daniel.alea.challenge.sqs.config.SqsConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SqsPublisherTest {

    @Mock
    AmazonSQSAsync sqsClient;

    @Mock
    SqsConfig sqsConfig;

    @Mock
    ObjectMapper jacksonMapper;

    @InjectMocks
    SqsPublisher sqsPublisher;

    @Test
    void publishesMessage() throws Exception {
        when(sqsConfig.getPokemonFanoutSqsUrl()).thenReturn("any-url");
        when(jacksonMapper.writeValueAsString(any())).thenReturn("any-serialization");

        Object o = new Object();

        sqsPublisher.publishMessage(o);

        verify(sqsConfig).getPokemonFanoutSqsUrl();
        verify(jacksonMapper).writeValueAsString(o);
        verify(sqsClient).sendMessageAsync("any-url", "any-serialization");
    }
}
