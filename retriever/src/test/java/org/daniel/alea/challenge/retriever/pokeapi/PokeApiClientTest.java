package org.daniel.alea.challenge.retriever.pokeapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.retriever.mapper.PokemonRetrieverMapper;
import org.daniel.alea.challenge.retriever.pokeapi.response.PagedPokemon;
import org.daniel.alea.challenge.retriever.pokeapi.response.PokemonPageResponse;
import org.daniel.alea.challenge.retriever.pokeapi.response.PokemonResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.util.List;

public class PokeApiClientTest {

    static MockWebServer mockWebServer;

    PokemonRetrieverMapper mapper = Mappers.getMapper(PokemonRetrieverMapper.class);

    WebClient webClient;

    PokeApiClient pokeApiClient;

    ObjectMapper objectMapper;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @BeforeEach
    void init() {
        objectMapper = new ObjectMapper();
        webClient = WebClient.builder().baseUrl(String.format("http://localhost:%s", mockWebServer.getPort())).build();
        pokeApiClient = new PokeApiClient(webClient, mapper);
    }

    @Test
    void fetchesPokemon() throws JsonProcessingException {
        PokemonResponse pokemonResponse = new PokemonResponse(1, "name", 2, 3, 4);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(pokemonResponse))
                .addHeader("Content-Type", "application/json"));

        Mono<PokemonDto> pokemonMono = pokeApiClient.getPokemon("name");
        PokemonDto expected = new PokemonDto(1, "name", 2, 3, 4);

        StepVerifier.create(pokemonMono)
                .expectNextMatches(pokemon -> pokemon.equals(expected))
                .verifyComplete();
    }

    @Test
    void fetchesPage() throws JsonProcessingException {
        PokemonPageResponse pageResponse = new PokemonPageResponse(1, List.of(new PagedPokemon("name")));
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(pageResponse))
                .addHeader("Content-Type", "application/json"));

        Mono<PokemonPage> pageMono = pokeApiClient.getPokemons(0, 5);
        PokemonPage expected = new PokemonPage(1, List.of(new PagedPokemon("name")));

        StepVerifier.create(pageMono)
                .expectNextMatches(page -> page.equals(expected))
                .verifyComplete();
    }
}
