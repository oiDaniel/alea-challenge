package org.daniel.alea.challenge.retriever.worker;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.retriever.pokeapi.PokeApiClient;
import org.daniel.alea.challenge.retriever.pokeapi.PokemonPage;
import org.daniel.alea.challenge.retriever.pokeapi.response.PagedPokemon;
import org.daniel.alea.challenge.retriever.publisher.SqsPublisher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class RetrieverWorkerTest {

    static final PokemonPage ANY_PAGE = PokemonPage.builder()
            .totalSize(1)
            .pokemons(List.of(new PagedPokemon("pikachu")))
            .build();
    static final PokemonDto ANY_POKEMON = new PokemonDto(1, "pikachu", 2, 3, 4);

    @Mock
    PokeApiClient pokeApiClient;

    @Mock
    SqsPublisher sqsPublisher;

    @InjectMocks
    RetrieverWorker retrieverWorker;

    @Test
    void throwsWhenCannotGetPage() {
        when(pokeApiClient.getPokemons(anyInt(), anyInt())).thenReturn(Mono.empty());

        assertThatThrownBy(() -> retrieverWorker.retrievePokemon())
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Could not fetch pokemon page");
        verify(pokeApiClient).getPokemons(anyInt(), anyInt());
    }

    @Test
    void throwsWhenCannotGetPokemonFromPage() {
        when(pokeApiClient.getPokemons(anyInt(), anyInt()))
                .thenReturn(Mono.just(ANY_PAGE));
        when(pokeApiClient.getPokemon(anyString()))
                .thenReturn(Mono.empty());

        assertThatThrownBy(() -> retrieverWorker.retrievePokemon())
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Could not fetch pokemon pikachu");
        verify(pokeApiClient).getPokemons(anyInt(), anyInt());
        verify(pokeApiClient).getPokemon("pikachu");
    }

    @Test
    void publishesMessageCorrectly() throws Exception {
        when(pokeApiClient.getPokemons(anyInt(), anyInt()))
                .thenReturn(Mono.just(ANY_PAGE));
        when(pokeApiClient.getPokemon(anyString()))
                .thenReturn(Mono.just(ANY_POKEMON));

        retrieverWorker.retrievePokemon();
        verify(pokeApiClient).getPokemons(anyInt(), anyInt());
        verify(pokeApiClient).getPokemon("pikachu");
        verify(sqsPublisher).publishMessage(ANY_POKEMON);
    }
}
