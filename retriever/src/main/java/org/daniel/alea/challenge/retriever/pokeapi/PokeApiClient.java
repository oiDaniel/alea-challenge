package org.daniel.alea.challenge.retriever.pokeapi;

import lombok.RequiredArgsConstructor;
import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.retriever.mapper.PokemonRetrieverMapper;
import org.daniel.alea.challenge.retriever.pokeapi.response.PokemonPageResponse;
import org.daniel.alea.challenge.retriever.pokeapi.response.PokemonResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class PokeApiClient {

    public static final int DEFAULT_PAGE_SIZE = 20;

    private final WebClient webClient;
    private final PokemonRetrieverMapper mapper;

    public Mono<PokemonDto> getPokemon(String name) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/{name}")
                        .build(name))
                .retrieve()
                .bodyToMono(PokemonResponse.class)
                .map(mapper::responseToDto);
    }

    public Mono<PokemonPage> getPokemons(int offset, int limit) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam("offset", offset)
                        .queryParam("limit", limit)
                        .build())
                .retrieve()
                .bodyToMono(PokemonPageResponse.class)
                .map(pageResponse -> PokemonPage.builder()
                        .totalSize(pageResponse.getCount())
                        .pokemons(pageResponse.getResults())
                        .build());
    }
}
