package org.daniel.alea.challenge.retriever.pokeapi.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class PokemonResponse {

    private int id;

    private String name;

    private int height;

    private int weight;

    @JsonProperty("base_experience")
    private int baseExperience;
}
