package org.daniel.alea.challenge.retriever.pokeapi;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;
import org.daniel.alea.challenge.retriever.pokeapi.response.PagedPokemon;

import java.util.List;

@Data
@Builder
@Setter(AccessLevel.NONE)
public class PokemonPage {

    private final int totalSize;

    private final List<PagedPokemon> pokemons;
}
