package org.daniel.alea.challenge.retriever.worker;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.daniel.alea.challenge.retriever.pokeapi.PokeApiClient;
import org.daniel.alea.challenge.retriever.pokeapi.PokemonPage;
import org.daniel.alea.challenge.retriever.publisher.SqsPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
public class RetrieverWorker {

    private static final long RETRIEVE_INTERVAL = 24 * 60 * 60 * 1000;

    private final PokeApiClient pokeApiClient;
    private final SqsPublisher sqsPublisher;

    @Scheduled(fixedRate = RETRIEVE_INTERVAL)
    public void retrievePokemon() {
        log.info("Retrieving all pokemon...");

        PokemonPage page;
        int offset = 0;
        do {
            page = pokeApiClient.getPokemons(offset, PokeApiClient.DEFAULT_PAGE_SIZE)
                    .blockOptional()
                    .orElseThrow(() -> {
                        log.error("Could not fetch pokemon page!");
                        return new IllegalStateException("Could not fetch pokemon page");
                    });

            log.info("Obtained pokemon page at offset {}, total count {}", offset, page.getTotalSize());

            page.getPokemons().parallelStream().forEach(pagedPokemon ->
                    pokeApiClient.getPokemon(pagedPokemon.getName())
                            .blockOptional()
                            .ifPresentOrElse(pokemon -> {
                                try {
                                    sqsPublisher.publishMessage(pokemon);
                                } catch (Exception e) {
                                    log.error("Error sending pokemon to writer queue", e);
                                }
                            }, () -> {
                                log.error("Error while fetching {}", pagedPokemon.getName());
                                throw new IllegalStateException("Could not fetch pokemon " + pagedPokemon.getName());
                            }));

            offset += PokeApiClient.DEFAULT_PAGE_SIZE;
        } while (offset < page.getTotalSize());

        log.info("Finished retrieving all pokemon!");
    }
}
