package org.daniel.alea.challenge.retriever.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class RetrieverConfig {

    @Value("${retriever.webclient.max.buffer.size.mb}")
    private int webClientMaxBufferSizeMb;

    @Bean
    public WebClient pokeWebClient() {
        return WebClient.builder().baseUrl("https://pokeapi.co/api/v2/pokemon")
                // https://stackoverflow.com/a/62543241/9148734
                .codecs(configurer -> configurer
                        .defaultCodecs()
                        .maxInMemorySize(webClientMaxBufferSizeMb * 1024 * 1024))
                .build();
    }
}
