package org.daniel.alea.challenge.retriever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "org.daniel.alea")
@EnableScheduling
public class RetrieverApp {

    public static void main(String[] args) {
        SpringApplication.run(RetrieverApp.class, args);
    }
}
