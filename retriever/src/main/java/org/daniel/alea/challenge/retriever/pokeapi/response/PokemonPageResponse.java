package org.daniel.alea.challenge.retriever.pokeapi.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class PokemonPageResponse {

    private int count;

    private List<PagedPokemon> results;
}
