package org.daniel.alea.challenge.retriever.publisher;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.daniel.alea.challenge.sqs.config.SqsConfig;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SqsPublisher {

    private final AmazonSQSAsync sqsClient;
    private final SqsConfig sqsConfig;
    private final ObjectMapper jacksonMapper;

    public void publishMessage(Object o) throws Exception {
        sqsClient.sendMessageAsync(sqsConfig.getPokemonFanoutSqsUrl(), jacksonMapper.writeValueAsString(o));
    }
}
