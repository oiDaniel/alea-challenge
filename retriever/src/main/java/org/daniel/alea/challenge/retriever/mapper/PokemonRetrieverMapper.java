package org.daniel.alea.challenge.retriever.mapper;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.retriever.pokeapi.response.PokemonResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PokemonRetrieverMapper {

    PokemonDto responseToDto(PokemonResponse pokemonResponse);
}
