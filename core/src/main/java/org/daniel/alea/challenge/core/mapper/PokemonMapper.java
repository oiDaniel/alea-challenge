package org.daniel.alea.challenge.core.mapper;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PokemonMapper {

    PokemonDto toDto(Pokemon pokemon);
    Pokemon fromDto(PokemonDto pokemonDto);
}
