package org.daniel.alea.challenge.core.repository;

import io.lettuce.core.api.sync.RedisCommands;
import lombok.RequiredArgsConstructor;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class PokemonRepository {

    private final RedisCommands<String, String> stringRedis;
    private final RedisCommands<String, Pokemon> pokemonRedis;

    public void save(Pokemon pokemon) {
        pokemonRedis.set(pokemon.getName(), pokemon);
        stringRedis.zadd(RedisSet.HEIGHT.getSetName(), pokemon.getHeight(), pokemon.getName());
        stringRedis.zadd(RedisSet.WEIGHT.getSetName(), pokemon.getWeight(), pokemon.getName());
        stringRedis.zadd(RedisSet.EXPERIENCE.getSetName(), pokemon.getBaseExperience(), pokemon.getName());
    }

    public List<Pokemon> getHeaviest(int limit) {
        return getPokemons(stringRedis.zrevrange(RedisSet.WEIGHT.getSetName(), 0, limit - 1));
    }

    public List<Pokemon> getTallest(int limit) {
        return getPokemons(stringRedis.zrevrange(RedisSet.HEIGHT.getSetName(), 0, limit - 1));
    }

    public List<Pokemon> getWithHighestBaseExperience(int limit) {
        return getPokemons(stringRedis.zrevrange(RedisSet.EXPERIENCE.getSetName(), 0, limit - 1));
    }

    private Pokemon getPokemon(String name) {
        return pokemonRedis.get(name);
    }

    private List<Pokemon> getPokemons(List<String> names) {
        return names.stream()
                .map(this::getPokemon)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
