package org.daniel.alea.challenge.core.service;

import lombok.RequiredArgsConstructor;
import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.mapper.PokemonMapper;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.daniel.alea.challenge.core.repository.PokemonRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PokemonService {

    private final PokemonRepository pokemonRepository;
    private final PokemonMapper pokemonMapper;

    public void save(PokemonDto pokemon) {
        pokemonRepository.save(pokemonMapper.fromDto(pokemon));
    }

    public List<PokemonDto> getHeaviest(int limit) {
        return toDtoList(pokemonRepository.getHeaviest(limit));
    }

    public List<PokemonDto> getTallest(int limit) {
        return toDtoList(pokemonRepository.getTallest(limit));
    }

    public List<PokemonDto> getWithHighestBaseExperience(int limit) {
        return toDtoList(pokemonRepository.getWithHighestBaseExperience(limit));
    }

    private List<PokemonDto> toDtoList(List<Pokemon> pokemons) {
        return pokemons.stream()
                .map(pokemonMapper::toDto)
                .collect(Collectors.toList());
    }
}
