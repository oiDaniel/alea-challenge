package org.daniel.alea.challenge.core.config;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.codec.ByteArrayCodec;
import io.lettuce.core.codec.RedisCodec;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

@Configuration
public class RedisConfig {

    @Bean
    public RedisClient redisClient() {
        return RedisClient.create("redis://localhost:6379/");
    }

    @Bean
    public RedisCommands<String, String> stringRedisCommands(RedisClient redisClient) {
        return redisClient.connect().sync();
    }

    @Bean
    public RedisCommands<String, Pokemon> pokemonRedisCommands(RedisClient redisClient) {
        // https://stackoverflow.com/a/70149743/9148734
        return redisClient.connect(new RedisCodec<String, Pokemon>() {
            private final ByteArrayCodec byteArrayCodec = new ByteArrayCodec();

            @Override
            public String decodeKey(ByteBuffer byteBuffer) {
                return Charset.defaultCharset().decode(byteBuffer).toString();
            }

            @Override
            public Pokemon decodeValue(ByteBuffer byteBuffer) {
                try (ObjectInputStream is =
                             new ObjectInputStream(new ByteArrayInputStream(byteArrayCodec.decodeValue(byteBuffer)))) {
                    return (Pokemon) is.readObject();
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }

            @Override
            public ByteBuffer encodeKey(String s) {
                return Charset.defaultCharset().encode(s);
            }

            @Override
            public ByteBuffer encodeValue(Pokemon pokemon) {
                try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
                     ObjectOutputStream os = new ObjectOutputStream(bos)) {
                    os.writeObject(pokemon);
                    return byteArrayCodec.encodeValue(bos.toByteArray());
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }).sync();
    }
}
