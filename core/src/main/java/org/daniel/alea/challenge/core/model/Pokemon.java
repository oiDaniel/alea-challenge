package org.daniel.alea.challenge.core.model;

import lombok.Value;

import java.io.Serializable;

@Value
public class Pokemon implements Serializable {

    int id;
    String name;
    int height;
    int weight;
    int baseExperience;
}
