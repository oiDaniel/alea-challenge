package org.daniel.alea.challenge.core.repository;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum RedisSet {
    WEIGHT("weight"),
    HEIGHT("height"),
    EXPERIENCE("experience");

    private final String setName;
}
