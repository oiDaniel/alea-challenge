package org.daniel.alea.challenge.core.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

@Data
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class PokemonDto {

    int id;
    String name;
    int height;
    int weight;
    int baseExperience;
}
