package org.daniel.alea.challenge.core.repository;

import io.lettuce.core.api.sync.RedisCommands;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PokemonRepositoryTest {

    static Pokemon ANY_POKEMON = new Pokemon(1, "name", 2, 3, 4);

    @Mock
    RedisCommands<String, String> stringRedis;

    @Mock
    RedisCommands<String, Pokemon> pokemonRedis;

    PokemonRepository pokemonRepository;

    @BeforeEach
    void setUp() {
        pokemonRepository = new PokemonRepository(stringRedis, pokemonRedis);
    }

    @Test
    void savesCorrectly() {
        pokemonRepository.save(ANY_POKEMON);

        verify(pokemonRedis).set(ANY_POKEMON.getName(), ANY_POKEMON);
        verify(stringRedis).zadd("height", 2, ANY_POKEMON.getName());
        verify(stringRedis).zadd("weight", 3, ANY_POKEMON.getName());
        verify(stringRedis).zadd("experience", 4, ANY_POKEMON.getName());
    }

    @Test
    void getsTallestCorrectly() {
        when(pokemonRedis.get(ANY_POKEMON.getName())).thenReturn(ANY_POKEMON);
        when(stringRedis.zrevrange(eq("height"), anyLong(), anyLong()))
                .thenReturn(List.of(ANY_POKEMON.getName()));

        List<Pokemon> result = pokemonRepository.getTallest(5);

        assertThat(result).containsExactly(ANY_POKEMON);
    }

    @Test
    void getsHeaviestCorrectly() {
        when(pokemonRedis.get(ANY_POKEMON.getName())).thenReturn(ANY_POKEMON);
        when(stringRedis.zrevrange(eq("weight"), anyLong(), anyLong()))
                .thenReturn(List.of(ANY_POKEMON.getName()));

        List<Pokemon> result = pokemonRepository.getHeaviest(5);

        assertThat(result).containsExactly(ANY_POKEMON);
    }

    @Test
    void getsHighestBaseExpCorrectly() {
        when(pokemonRedis.get(ANY_POKEMON.getName())).thenReturn(ANY_POKEMON);
        when(stringRedis.zrevrange(eq("experience"), anyLong(), anyLong()))
                .thenReturn(List.of(ANY_POKEMON.getName()));

        List<Pokemon> result = pokemonRepository.getWithHighestBaseExperience(5);

        assertThat(result).containsExactly(ANY_POKEMON);
    }
}
