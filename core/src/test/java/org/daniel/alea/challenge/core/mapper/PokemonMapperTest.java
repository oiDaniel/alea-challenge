package org.daniel.alea.challenge.core.mapper;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class PokemonMapperTest {

    PokemonMapper mapper = Mappers.getMapper(PokemonMapper.class);

    @Test
    void mapsToDtoCorrectly() {
        assertThat(mapper.toDto(new Pokemon(1, "a", 2, 3, 4)))
                .isEqualTo(new PokemonDto(1, "a", 2, 3, 4));
    }

    @Test
    void mapsToModelCorrectly() {
        assertThat(mapper.fromDto(new PokemonDto(1, "a", 2, 3, 4)))
                .isEqualTo(new Pokemon(1, "a", 2, 3, 4));
    }
}
