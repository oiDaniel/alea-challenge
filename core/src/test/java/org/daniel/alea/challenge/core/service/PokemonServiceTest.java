package org.daniel.alea.challenge.core.service;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.mapper.PokemonMapper;
import org.daniel.alea.challenge.core.model.Pokemon;
import org.daniel.alea.challenge.core.repository.PokemonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PokemonServiceTest {

    static PokemonDto ANY_POKEMON_DTO = new PokemonDto(1, "name", 2, 3, 4);
    static Pokemon ANY_POKEMON = new Pokemon(1, "name", 2, 3, 4);

    @Mock
    PokemonRepository pokemonRepository;

    @Mock
    PokemonMapper mapper;

    @InjectMocks
    PokemonService pokemonService;

    @BeforeEach
    void setUp() {
        when(mapper.toDto(ANY_POKEMON)).thenReturn(ANY_POKEMON_DTO);
    }

    @Test
    void returnsCorrectlyHeaviest() {
        when(pokemonRepository.getHeaviest(anyInt())).thenReturn(List.of(ANY_POKEMON));

        assertThat(pokemonService.getHeaviest(5)).containsExactly(ANY_POKEMON_DTO);
        verify(pokemonRepository).getHeaviest(5);
        verify(mapper).toDto(ANY_POKEMON);
    }

    @Test
    void returnsCorrectlyTallest() {
        when(pokemonRepository.getTallest(anyInt())).thenReturn(List.of(ANY_POKEMON));

        assertThat(pokemonService.getTallest(5)).containsExactly(ANY_POKEMON_DTO);
        verify(pokemonRepository).getTallest(5);
        verify(mapper).toDto(ANY_POKEMON);
    }


    @Test
    void returnsCorrectlyMostExp() {
        when(pokemonRepository.getWithHighestBaseExperience(anyInt())).thenReturn(List.of(ANY_POKEMON));

        assertThat(pokemonService.getWithHighestBaseExperience(5)).containsExactly(ANY_POKEMON_DTO);
        verify(pokemonRepository).getWithHighestBaseExperience(5);
        verify(mapper).toDto(ANY_POKEMON);
    }
}
