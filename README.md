# Alea Tech Challenge

In this repository you may find the solution for the Alea tech challenge which consists of consuming the
[PokeApi](https://pokeapi.co/) and find the:

1. Heaviest pokemons.
2. Tallest pokemons.
3. Pokemons with the highest base experience.

## Requirements

- Java 11
- Maven
- Docker

## How to Run

Set up the infrastructure and compile the project:

- `docker compose up`
- `mvn install`

After compiling, launch these commands separately:

- `mvn exec:java -pl writer`
- `mvn exec:java -pl retriever`
- `mvn exec:java -pl reader`

## How to Use

After all the services are up and running¹, you can `GET` the following endpoints:

- `http://localhost:8080/pokemon/tallest` for the tallest ones.
- `http://localhost:8080/pokemon/heaviest` for the heaviest ones.
- `http://localhost:8080/pokemon/highestBaseExp` for the ones with the highest base experience.

Additionally, you can add a `limit` query parameter to obtain more than 5 pokemons at a time. E.g.
`http://localhost:8080/pokemon/tallest?limit=10`

¹: *up and running* **and** the database has been populated. More on this later. 

## Design

### Preface/Context

In this document, "eventual consistency" means being eventually in sync with all the data PokeApi has. Due to the nature
of the problem, it is extremely hard to always be in sync. That single roadblock was the main driver for the design that
will be presented.

Given that:

1. At least a thousand HTTP calls are needed to obtain all the pokemon data.
1. A few dozen more are needed to get the paginated information.
1. This is a slow process.

While taking into account the CAP theorem, a decision (or sacrifice) was made: have an *available* and
*partition tolerant* system.

![CAP theorem](https://i.imgur.com/l1gQZY4.png)

As said before, the nature of the problem forces the data to come in slowly, and the system will most likely be out of
sync during multiple points in time. So, having a strong consistency is out of the question right at the start.

### Objectives and Approach

The main driver for this system design was the desire to have system that is:

1. Highly available.
1. Partition tolerant.
1. Highly scalable.
1. Eventually consistent.
1. Stateless.
1. Fast.

Given these desires, an architecture following the
[Command-Query Responsibility Segregation](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs) pattern
was chosen.

The architecture of this solution is as follows:

![Architecture](https://i.imgur.com/ih6ycxU.png)

It consists of three microservices, an AWS SQS and Redis database. Each of these components will be explained below.
These microservices are rather straightforward and only perform a single task each.

### Retriever

This service is the "entrypoint" of the system and is in charge of obtaining all the existing pokemons from the PokeApi.
It queries the API, obtains the pokemons and then publishes them to a SQS, where the **Writer** will process them.
Pokemons can be classified as data that barely ever changes (when a new Pokemon game is released?) and therefore the
PokeApi is consulted every 24 hours. Upon launch and every 24 hours, all the pokemons are fetched.

### Writer

The **Writer** is sitting idle listening to the queue, waiting for messages to be pushed. A single message of the queue
contains the metadata of a pokemon, such as its name, weight, height, etc. When a message is received, this microservice
stores it in Redis.

### Reader

This is the program the user interacts with. It exposes three endpoints to obtain the desired pokemons (the heaviest,
the tallest...). The pokemons are read from the Redis instance and returned to the user, ordered by the required
characteristic (height, weight...).

### SQS

SQS was chosen as a way to asynchronously communicate the **Retriever** and **Writer** microservices due to its
simplicity and high throughput.

### Redis

Redis was chosen as the mean to store the pokemons due to its high speed, *sorted sets* (`O(log(n))` complexity for
storing and retrieving pokemons), and key-value storing capabilities. Specifically, pokemons are stored in this manner:

- A full pokemon (with all its metadata) is stored in the "global" set
  (equivalent to `SET charmander '{"id":"2","weight":23}'` and `GET charmander`).
- There are three more sorted sets, where only the names of the pokemons are stored.
    - A sorted set where pokemons are sorted by their height.
    - A sorted set where pokemons are sorted by their weight.
    - A sorted set where pokemons are sorted by their base experience.

This way, when querying *which are the tallest pokemons?* a `ZRANGE` command is executed on the sorted sets to retrieve
the names of the pokemons, and then a `GET` command is performed on the "global" set to retrieve the rest of the
information.

## Advantages and Disadvantages

This is system is definitely not perfect, it is strong is some aspects and weak in others:

### Advantages

- Decoupled system.
- As all components are stateless, they can be independently scaled if there is too much load, for example, if there are
  many pokemons in the SQS, the **Writer** can have more instances spawned. The same happens with the **Reader**, if
  there are too many HTTP requests.
- The failure of one piece does not suppose the total failure of the system. Should the **Retriever** or the **Writer**
  crash, the **Reader** could still serve the user the data about the pokemons, the worse thing that could happen in
  this scenario is presenting the user outdated data.

### Disadvantages

- Complex, over-engineered. Yes, without a doubt.
- Eventually consistent, not strongly consistent. Even though it was a design choice, it is very possible that if new
  pokemons are released, the system will not show them while they're still being fetched by the **Retriever** or written
  by the **Writer**, or show nothing at all during the first execution of the **Retriever** and **Writer** (when the
  Redis database is empty).

## The Future Ahead

There were a few things that were not done in this implementation due to time constraints, and those were:

1. Integration and E2E tests.
1. Docker-izing the services. Instead, every service has to be run on the host machine and the ports 8080, 8081, and
   8082 have to be available.
1. Using a write cache. All the pokemons that are fetched by the **Retriever** are always sent to the **Writer**. By
   using a cache, unnecessary writes to the Redis instance could be avoided. I.e., the data of Pikachu is not going to
   change, therefore it does not have to be rewritten.
1. During the startup phase of the **Writer**, some exceptions come up. They do not affect the execution of the program,
   but they're present. This is caused due to the `spring-aws-starter` dependency, where it looks for some configuration
   present when running in an AWS EC2 instance. Since this is not the case, it fails to do so. Fixing this could be
   wanted.
