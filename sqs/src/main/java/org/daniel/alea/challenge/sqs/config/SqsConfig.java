package org.daniel.alea.challenge.sqs.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class SqsConfig {

    @Value("${pokemon.fanout.sqs.url}")
    private String pokemonFanoutSqsUrl;

    @Bean
    @Primary
    public AWSCredentialsProvider awsCredentials() {
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials("accessKey", "secretKey"));
    }

    @Bean
    @Primary
    public AwsClientBuilder.EndpointConfiguration awsEndpointConfig() {
        return new AwsClientBuilder.EndpointConfiguration("http://localhost:4566", "eu-west-1");
    }

    @Bean
    @Primary
    public AmazonSQSAsync amazonSQSAsync(AWSCredentialsProvider awsCredentials,
                                         AwsClientBuilder.EndpointConfiguration awsEndpointConfig) {
        return AmazonSQSAsyncClientBuilder.standard()
                .withEndpointConfiguration(awsEndpointConfig)
                .withCredentials(awsCredentials)
                .build();
    }

    public String getPokemonFanoutSqsUrl() {
        return pokemonFanoutSqsUrl;
    }
}
