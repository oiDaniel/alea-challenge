package org.daniel.alea.challenge.writer.consumer;

import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.service.PokemonService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PokemonConsumerTest {

    static PokemonDto ANY_POKEMON_DTO = new PokemonDto(1, "name", 2, 3, 4);

    @Mock
    PokemonService pokemonService;

    @InjectMocks
    PokemonConsumer pokemonConsumer;

    @Test
    void consumesCorrectly() {
        pokemonConsumer.consume(ANY_POKEMON_DTO);

        verify(pokemonService).save(ANY_POKEMON_DTO);
    }
}
