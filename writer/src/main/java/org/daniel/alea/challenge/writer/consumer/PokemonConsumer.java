package org.daniel.alea.challenge.writer.consumer;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.daniel.alea.challenge.core.dto.PokemonDto;
import org.daniel.alea.challenge.core.service.PokemonService;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Log4j2
public class PokemonConsumer {

    private final PokemonService pokemonService;

    @SqsListener(value = "${pokemon.fanout.sqs.url}", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void consume(PokemonDto pokemonDto) {
        log.info("Received pokemon {}", pokemonDto.getName());
        pokemonService.save(pokemonDto);
    }
}
