package org.daniel.alea.challenge.writer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "org.daniel.alea")
public class WriterApp {

    public static void main(String[] args) {
        SpringApplication.run(WriterApp.class, args);
    }
}
